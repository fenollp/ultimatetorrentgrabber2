function onClickHandler(info, tab) {
    var searchstring = info.selectionText;
    var regex = /[a-fA-F0-9]{40}/;
    if (! regex.test(searchstring))
        return;

    if (info.menuItemId == 'magnet') {
        chrome.tabs.create({
            url: 'magnet:?xt=urn:btih:' + searchstring
            // https://tinytorrent.net/best-torrent-tracker-list-updated

                + '&tr=http%3A%2F%2Fagusiq-torrents.pl%3A6969'
                + '&tr=http%3A%2F%2Fasnet.pw%3A2710'
                + '&tr=http%3A%2F%2Ffxtt.ru%3A80'
                + '&tr=http%3A%2F%2Fgrifon.info%3A80'
                + '&tr=http%3A%2F%2Fns349743.ip-91-121-106.eu%3A80'
                + '&tr=http%3A%2F%2Fpt.lax.mx%3A80'
                + '&tr=http%3A%2F%2Fretracker.bashtel.ru%3A80'
                + '&tr=http%3A%2F%2Fretracker.mgts.by%3A80'
                + '&tr=http%3A%2F%2Fretracker.spark-rostov.ru%3A80'
                + '&tr=http%3A%2F%2Fretracker.telecom.by%3A80'
                + '&tr=http%3A%2F%2Fshare.camoe.cn%3A8080'
                + '&tr=http%3A%2F%2Ft.nyaatracker.com%3A80'
                + '&tr=http%3A%2F%2Ftorrentsmd.eu%3A8080'
                + '&tr=http%3A%2F%2Ftorrentsmd.me%3A8080'
                + '&tr=http%3A%2F%2Ftr.kxmp.cf%3A80'
                + '&tr=http%3A%2F%2Ftracker.city9x.com%3A2710'
                + '&tr=http%3A%2F%2Ftracker.devil-torrents.pl%3A80'
                + '&tr=http%3A%2F%2Ftracker.electro-torrent.pl%3A80'
                + '&tr=http%3A%2F%2Ftracker.tfile.co%3A80'
                + '&tr=http%3A%2F%2Ftracker.tfile.me%3A80'
                + '&tr=https%3A%2F%2Fopen.kickasstracker.com%3A443'
                + '&tr=https%3A%2F%2Ftracker.bt-hash.com%3A443'
                + '&tr=udp%3A%2F%2Fbt.aoeex.com%3A8000'
                + '&tr=udp%3A%2F%2Fbt.xxx-tracker.com%3A2710'
                + '&tr=udp%3A%2F%2Finferno.demonoid.pw%3A3418'
                + '&tr=udp%3A%2F%2Fopen.demonii.com%3A1337'
                + '&tr=udp%3A%2F%2Fopen.stealth.si%3A80'
                + '&tr=udp%3A%2F%2Fpeerfect.org%3A6969'
                + '&tr=udp%3A%2F%2Fretracker.lanta-net.ru%3A2710'
                + '&tr=udp%3A%2F%2Fretracker.nts.su%3A2710'
                + '&tr=udp%3A%2F%2Fsantost12.xyz%3A6969'
                + '&tr=udp%3A%2F%2Ftc.animereactor.ru%3A8082'
                + '&tr=udp%3A%2F%2Fthetracker.org%3A80'
                + '&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.cyberia.is%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.cypherpunks.ru%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.doko.moe%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.dutchtracking.com%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.halfchub.club%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337'
                + '&tr=udp%3A%2F%2Ftracker.justseed.it%3A1337'
                + '&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80'
                + '&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337'
                + '&tr=udp%3A%2F%2Ftracker.swateam.org.uk%3A2710'
                + '&tr=udp%3A%2F%2Ftracker.tiny-vps.com%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.torrent.eu.org%3A451'
                + '&tr=udp%3A%2F%2Ftracker.tvunderground.org.ru%3A3218'
                + '&tr=udp%3A%2F%2Ftracker.vanitycore.co%3A6969'
                + '&tr=udp%3A%2F%2Ftracker.zer0day.to%3A1337'
                + '&tr=udp%3A%2F%2Ftracker2.christianbro.pw%3A6969'
                + '&tr=udp%3A%2F%2Fulfbrueggemann.no-ip.org%3A6969'
                + '&tr=udp%3A%2F%2Fwambo.club%3A1337'
                + '&tr=udp%3A%2F%2Fzephir.monocul.us%3A6969'

        });
        setTimeout(function() {
            chrome.tabs.getSelected(null, function(tab) {
                chrome.tabs.remove(tab.id);
            });
        }, 1000);
    } else if (info.menuItemId === 'itorrents') {
        chrome.tabs.create({
            url: 'https://itorrents.org/torrent/' + searchstring + '.torrent'
        });
    } else {
        alert('Hash values are 40 characters in length');
    }
};

chrome.contextMenus.onClicked.addListener(onClickHandler);

chrome.runtime.onInstalled.addListener(function() {
    var contexts = ['selection'];
    for (var i = 0; i < contexts.length; i++) {
        var context = contexts[i];
        chrome.contextMenus.create({
            title: 'Download %s via Magnet',
            contexts: [context],
            id: 'magnet',
        });
        chrome.contextMenus.create({
            title: 'Download %s via iTorrents',
            contexts: [context],
            id: 'itorrents'
        });
    }
});
